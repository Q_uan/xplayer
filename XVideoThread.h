#ifndef XVIDEOTHREAD_H
#define XVIDEOTHREAD_H

#include <QThread>
#include <QMutex>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/video.hpp>
#include <QDebug>



using namespace cv;
class XVideoThread:public QThread
{
    Q_OBJECT
public:
    static XVideoThread * Get();//单例
    ~XVideoThread();
    void run();
    bool Open(const QString file);
    bool Open2(const QString file);
    void Play() {mutex.lock();isPlay=true;mutex.unlock();}
    void Pause(){mutex.lock();isPlay=false;mutex.unlock();}
    void stop();
    double GetPos(void);
    //跳转视频
    ///@para frame int 帧位置
    bool Seek(int frame);
    bool Seek(double pos);
    bool StartSave(const std::string filename,int width=0,int height=0,bool isColor=true);//开始保存视频
    void StopSave();//停止保存视频，写入视频帧的索引
    void SetMark(Mat mark);

signals:
    void ViewImage1(cv::Mat mat);
    void ViewImage2(cv::Mat mat);
    void ViewDes(cv::Mat mat);
    void SaveEnd();
public:
    bool runFlag=true;
    QMutex mutex;
    int fps=0;
    int width=0;
    int height=0;
    int width2=0;
    int height2=0;
    int totalMs=0;
    std::string srcfile;
    std::string desfile;
    bool isWrite =false;//是否开启写视频
    bool isPlay=false;
    cv::Mat mark;
    VideoWriter vw;
    int begin=0;
    int end=999;
    void SetBegin(double p);
    void SetEnd(double p);

     XVideoThread();

};
Q_DECLARE_METATYPE(cv::Mat);

#endif // XVIDEOTHREAD_H
