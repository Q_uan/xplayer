#include "XVideoUIClass.h"
#include "ui_XVideoUIClass.h"
#include <QFileDialog>
#include "XVideoThread.h"
#include <QThread>
#include "XFilter.h"
#include <iostream>
#include "XAudio.h"
static bool pressSlider=false;
static bool isExport=false;
static bool isPy=false;
static bool isClip=false;
static bool isColor=true;
static bool isMark=true;
static bool isBlend=false;
static bool isMerge=false;
XVideoUIClass::XVideoUIClass(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::XVideoUIClass)
{
    ui->setupUi(this);
    this->move(150,50);
    setWindowFlags(Qt::FramelessWindowHint);

    timer.setInterval(40);
    connect(&timer,SIGNAL(timeout()),this,SLOT(FreshSlider()));
    timer.start();
    connect(XVideoThread::Get(),SIGNAL(SaveEnd()),this,SLOT(ExportEnd()));
    Pause();//使界面默认为暂停
    connect(XVideoThread::Get(),SIGNAL(ViewImage2(cv::Mat )),ui->src2,SLOT(SetImage(cv::Mat )));
}

XVideoUIClass::~XVideoUIClass()
{
    delete ui;
}
void XVideoUIClass::Open()
{
    QString name =QFileDialog::getOpenFileName(this,QString::fromLocal8Bit("请选择视频文件"));
    if(name.isEmpty())
        return ;
   XVideoThread *thread= XVideoThread::Get();
   if(thread->Open(name))
   {
       if(!thread->isRunning())
       {
            connect(thread,SIGNAL(ViewImage1(cv::Mat )),ui->src1,SLOT(SetImage(cv::Mat )));
            connect(thread,SIGNAL(ViewDes(cv::Mat )),ui->des,SLOT(SetImage(cv::Mat )));
            thread->start();
       }
   }
   else
   {

   }
}
void XVideoUIClass::close()
{
    if( XVideoThread::Get()->isRunning())
    {
        XVideoThread::Get()->stop();
    }
    wait();
    QWidget::close();
}
void XVideoUIClass::Set()
{
    XFilter::GetInstance()->Clear();
    //对比度和亮度
    if( (ui->bright->value()>0)&&(ui->contrast->value()>1) )
    {

        XFilter::GetInstance()->Add(XFilter::XTask
                                    {XFilter::XTASK_GAIN,
                                        {(double)ui->bright->value(),
                                          ui->contrast->value()}
                                    }
                                   );
    }

    //图形旋转
    switch(ui->rotate->currentIndex())
    {
        case 1:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_ROTATE90});
            break;
        case 2:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_ROTATE180});
            break;
        case 3:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_ROTATE270});
            break;
        default:
            break;
    }
    //图像镜像

    switch(ui->flip->currentIndex())
    {
        case 1:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_FLIPX});
            break;
        case 2:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_FLIPY});
            break;
        case 3:
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_FLIPXY});
            break;
    }
    //resize
    if((isClip==false)&&(isMerge==false))
    {
        double h=static_cast<double>(ui->outheight->value() );
        double w=static_cast<double>(ui->outwidth->value()  );
        if(( ui->outheight->value()>0)&&
           ( ui->outheight->value()>0))
        {
            XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_RESIZE,{w,h}});
        }
    }
    //pyrUp or pyRDown
    int down=ui->pyrdown->value();
    int up=ui->pyrup->value();
    if(up>0)
    {
         isPy=true;
         XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_PYUP,{(double)up}});
         int w=XVideoThread::Get()->width;
         int h=XVideoThread::Get()->height;
         for(int i=0;i<up;i++)
         {
             h=h*2;
             w=w*2;
         }
         ui->outheight->setValue(h);
         ui->outwidth->setValue(w);
    }
    if(down>0)
    {
         isPy=true;
         XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_PYDOWN,{(double)down}});
         int w=XVideoThread::Get()->width;
         int h=XVideoThread::Get()->height;
         for(int i=0;i<down;i++)
         {
             h=h/2;
             w=w/2;
         }
         ui->outheight->setValue(h);
         ui->outwidth->setValue(w);
    }
    /******图像裁剪(x,y,w,h)*****/
    double clip_x=ui->clip_x->value();
    double clip_y=ui->clip_y->value();
    double clip_w=ui->clip_w->value();
    double clip_h=ui->clip_h->value();
    if((clip_x+clip_y+clip_w+clip_h)>0.0001)
    {
        isClip=true;
        XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_CLIP,{clip_x,clip_y,clip_w,clip_h}});
        double w=XVideoThread::Get()->width;
        double h=XVideoThread::Get()->height;
         XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_RESIZE,{w,h}});

    }
    /********gray**********/
    if(ui->color_combox->currentIndex()==1)
    {
      XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_GRAY});
      isColor=false;
    }
    else
    {
        isColor=true;
    }
    /**************************/
    if(isMark)
    {
        double x=ui->mx->value();
        double y=ui->my->value();
        double a=ui->ma->value();
        XFilter::GetInstance()->Add(XFilter::XTask{ XFilter::XTASK_MARK,{x,y,a}});
    }
    /******图像融合***************/
    if(isBlend)
    {
        double a=ui->ba->value();
        XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_BLEND,{a}});
    }
    /*******merge*********************/
    if(isMerge)
    {
        XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_MERGE});
    }
}
void XVideoUIClass::FreshSlider()
{
    if(pressSlider) return;//如果按下滑动条就返回
    double pos=XVideoThread::Get()->GetPos();
    ui->playSlider->setValue(  static_cast<int>(pos*1000));
}

void XVideoUIClass::SliderPress()
{
    pressSlider=true;
}
void XVideoUIClass::SliderRelease()
{
    pressSlider=false;
}
void XVideoUIClass::SetPos(int pos)
{
  XVideoThread::Get()->Seek(  static_cast<double>(pos/1000.0) );
}
void XVideoUIClass::ExportEnd()//导出结束
{
    int ss=0;
    int t=0;

    isExport=false;
    ui->exportButton->setText("start export");

    std::string src=XVideoThread::Get()->srcfile;
    std::string des=XVideoThread::Get()->desfile;


    ss=XVideoThread::Get()->totalMs*(ui->leftSlider->value()/1000.0);
    int end=XVideoThread::Get()->totalMs*(ui->rightSlider->value()/1000.0);
    t=end-ss;
    XAudio::GetInstance()->ExportA(src,"ExportA.aac",ss,t);

    XAudio::GetInstance()->Merge(des,"ExportA.aac","ExportALL.mp4");


    QFile::remove(des.c_str());
    QFile::rename("ExportALL.mp4",des.c_str());
    QFile::remove("ExportA.aac");


}
void XVideoUIClass::Play()
{
    ui->pauseButton->show();
    ui->pauseButton->setGeometry(ui->playButton->geometry());
    ui->playButton->hide();
    XVideoThread::Get()->Play();
}
void XVideoUIClass::Pause()
{
    ui->pauseButton->hide();
    ui->playButton->show();
    XVideoThread::Get()->Pause();
}
void XVideoUIClass::LeftSlider(int pos)
{
    XVideoThread::Get()->SetBegin(pos/1000.0);
}
void XVideoUIClass::RightSlider(int pos)
{
    XVideoThread::Get()->SetEnd(pos/1000.0);
}

void XVideoUIClass::on_playSlider_actionTriggered(int action)
{

}
void  XVideoUIClass::Export()
{

  if(isExport)
  {
      XVideoThread::Get()->stop();
      isExport=false;
      return  ;
  }
  QString name=QFileDialog::getSaveFileName(this,"save","out1.avi");
  if(name.isEmpty())
      return ;
  qDebug()<<"name is "<<name<<endl;
  std::string filename=name.toLocal8Bit().data();

  int w=ui->outwidth->value();
  int h=ui->outheight->value();
  if((w==0)&&(h==0))
  {
    w=640;
    h=360;
  }
/********图形旋转兼容处理***********/
 if((ui->rotate->currentIndex()&0x01)!=0)
 {
    w=w^h;
    h=w^h;
    w=w^h;
 }
 /*******************/
  if(XVideoThread::Get()->StartSave(filename,w,h,isColor))
  {
      isExport=true;
      ui->exportButton->setText("Stop Export");
  }
}
void XVideoUIClass::Mark()
{
    isMark=false;
    isBlend=false;
    isMerge=false;
    QString name=QFileDialog::getOpenFileName(this,"select image:");
    if(name.isEmpty())
        return ;
    std::string file=name.toLocal8Bit().data();
    cv::Mat mark=imread(file);
    if(mark.empty())
        return  ;
    XVideoThread::Get()->SetMark(mark);
    isMark=true;
}
void  XVideoUIClass::Blend()
{
    isMark=false;
    isBlend=false;
    isMerge=false;
    QString name=QFileDialog::getOpenFileName(this,"selsect image");
    if(name.isEmpty())
    {
        return ;
    }
    std::string file=name.toLocal8Bit().data();
    isBlend=XVideoThread::Get()->Open2(QString::fromStdString(file));
}
 void XVideoUIClass::Merge()
 {
     isMark=false;
     isBlend=false;
     isMerge=false;
     QString name=QFileDialog::getOpenFileName(this,"select video:");
     if(name.isEmpty())
     {
         return ;
     }

     std::string file=name.toLocal8Bit().data();
     isMerge=XVideoThread::Get()->Open2(QString::fromStdString(file));
     if(isMerge)
     {
         double h2=XVideoThread::Get()->height2;
         double h1=XVideoThread::Get()->height;
         int w=XVideoThread::Get()->width2*(h2/h1);
         XFilter::GetInstance()->Add(XFilter::XTask{XFilter::XTASK_MERGE});
         ui->outwidth->setValue(XVideoThread::Get()->width+w);
         ui->outheight->setValue(h1);
     }

 }
