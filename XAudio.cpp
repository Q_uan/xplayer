#include "XAudio.h"
#include <iostream>
using namespace  std;
#include <QMutex>
struct XTime
{
    XTime(int tms)
    {
        h=(tms/1000)/3600;
        m=((tms/1000)%3600)/60;
        s=(tms/1000)%(3600*60);
        ms=tms%1000;
    }
    std::string Tostring()
    {
        char buf[16]={0};
        sprintf(buf,"%d:%d:%d.%d",h,m,s,ms);
        return buf;
    }
    int h=0;
    int m=0;
    int s=0;
    int ms=0;
};
class CXAudio:public XAudio
{
public:
    QMutex mutex;
    CXAudio(){}
    ~CXAudio(){}
     bool ExportA(std::string src,std::string out,int beginMs,int outMs)
     {
        //ffmpeg -i test.mp4 -acodec copy -vn -y test.aac
        //ffmpeg -i test.mp4 -ss 00:01:10.112 -t 00:02:10.100 -acodec copy -vn -y test.aac
         string cmd=" ffmpeg ";
         cmd+=" -i ";
         cmd+=src ;
         cmd+=" " ;
         if(beginMs>0)
         {
             cmd+=" -ss ";
             XTime xt(beginMs);
             cmd+=xt.Tostring();
         }
         if(outMs>0)
         {
             cmd+=" -t ";
             XTime xt(outMs);
             cmd +=xt.Tostring();
         }
         cmd+=" -acodec copy ";
         cmd+=" -vn -y ";
         cmd+=out;
         cout<<cmd<<endl;

         mutex.lock();
         system(cmd.c_str());
         mutex.unlock();

         return true;
     }
     bool Merge(std::string v,std::string a,std::string out)
     {
        // ffmpeg -i test.avi -i test.aac -c copy out.avi
         string cmd = "ffmpeg -i ";
         cmd+=v;
         cmd+=" -i ";
         cmd+=a;
         cmd+=" -c copy ";
         cmd +=out;
         cout<<cmd<<endl;

         mutex.lock();
         system(cmd.c_str());
         mutex.unlock();
         return true;
     }

};
XAudio::XAudio()
{

}
XAudio::~XAudio()
{

}
XAudio*XAudio:: GetInstance()
{
    static CXAudio ca;
    return &ca;
}

