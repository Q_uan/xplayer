#ifndef XVIDEOWIDGET_H
#define XVIDEOWIDGET_H

#include <QtCore/QObject>
#include <QtCore/qglobal.h>
#include <QOpenGLWidget>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
using namespace cv;
class XVideoWidget : public QOpenGLWidget
{
    Q_OBJECT
public:
    XVideoWidget(QWidget *p);
    void paintEvent(QPaintEvent *e);
    virtual ~XVideoWidget();
public slots:
    void SetImage(cv::Mat mat);
protected:
    QImage img;
    uchar *buf=nullptr;
};

#endif // XVIDEOWIDGET_H
