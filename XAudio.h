#ifndef XAUDIO_H
#define XAUDIO_H

#include<string>

class XAudio
{
public:
    static XAudio* GetInstance();

     /*导出音频文件
      * src            源文件
      * out            输出的音频文件
      * beginMs        开始音频位置毫秒
      * outMs          音频时长
        */
    virtual bool ExportA(std::string src,std::string out ,int beginMs,int outMs)=0;
    /*合并音视频 v 视频文件 a 音频文件   out 输出文件*/
    virtual bool Merge(std::string v,std::string a,std::string out)=0;
    XAudio();
    virtual ~XAudio();
};

#endif // XAUDIO_H
