#include "XVideoWidget.h"
#include <QPainter>
XVideoWidget::XVideoWidget(QWidget *p): QOpenGLWidget (p)
{

}

void XVideoWidget::paintEvent(QPaintEvent *e)
{

    QPainter painter;
    painter.begin(this);
    painter.drawImage(QPoint(0,0),img);
    painter.end();


}
 XVideoWidget::~XVideoWidget()
{
    delete buf;
}
void  XVideoWidget::SetImage(cv::Mat mat)
{
    QImage::Format fmt;
    int pixSize;
    if(mat.type()==CV_8UC1)
    {
        fmt=QImage::Format_Grayscale8;
        pixSize=1;
    }
    else
    {
        fmt=QImage::Format_RGB888;
        pixSize=3;
    }

    if(img.isNull()||img.format()!=fmt)
    {
        delete img.bits();
        uchar *buf=new uchar[width()*height()*pixSize];
        img=QImage(buf,width(),height(),fmt);
    }

    Mat des;
    cv::resize(mat,des,Size(  img.size().width(),img.size().height() ) );
    if(pixSize!=1)
    {
         cv::cvtColor(des,des,COLOR_BGR2RGB);
    }
    memcpy(img.bits(),des.data,des.cols*des.rows*des.elemSize());

    update();

}
