#ifndef XIMAGEPRO_H
#define XIMAGEPRO_H

#include <QtCore/qglobal.h>
#include <opencv2/core.hpp>
class XImagePro
{
public:
    void Set(cv::Mat mat1,cv::Mat mat2);// 设置原图，会清理处理结果
    cv::Mat Get()  {return des;}
    //设置亮度和对比度
    //bright  double 0~100
    //constrast int  1.0~3.0
    /********************/
    void Gain(double bright,double constrast);
    /********************/
    void Rotate90();
    void Rotate180();
    void Rotate270();
    /********************/
    void FlipX();
    void FlipY();
    void FlipXY();
    /********************/
    void Resize(int width,int height);
    /*****图像金字塔*********************/
    void PyDown(int count);
    void PyUp(int count);
    /****************************/
    void Clip(int x,int y,int w,int h);
    /***********************************/
    void Gray();
    /***********水印*************/
    void Mark(int x,int y,double a);
    /**********融合******************/
    void Blend(double a );
    /***********并排合并**********************/
    void Merge();

    XImagePro();
    ~XImagePro();
private:
    //原图
    cv::Mat src1, src2;
    //目标图
    cv::Mat des;
};

#endif // XIMAGEPRO_H
