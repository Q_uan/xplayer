#ifndef XFILTER_H
#define XFILTER_H
#include <opencv2/core.hpp>
#include <QtCore/qglobal.h>
#include <QMutex>
#include <QObject>
class XFilter:public QObject
{
    Q_OBJECT
public:
    enum XTaskType
    {
        XTASK_NONE,
        XTASK_GAIN,//调整亮度对比度

        XTASK_ROTATE90,
        XTASK_ROTATE180,
        XTASK_ROTATE270,

        XTASK_FLIPX,
        XTASK_FLIPY,
        XTASK_FLIPXY,

        XTASK_RESIZE,

        XTASK_PYDOWN,
        XTASK_PYUP,

        XTASK_CLIP,

        XTASK_GRAY,

        XTASK_MARK,

        XTASK_BLEND,

        XTASK_MERGE,
    };
    struct XTask
    {
       XTaskType type;
       std::vector<double> para;
    };
    virtual cv::Mat Filter(cv::Mat mat1,cv::Mat mat2)=0;
    virtual void Add(XTask task)=0;
    virtual void Clear()=0;
    static XFilter * GetInstance();
    XFilter();
    virtual ~XFilter();
};

class CXFilter  :public XFilter
{
    Q_OBJECT
public:
     cv::Mat Filter(cv::Mat mat1,cv::Mat mat2);
     void Add(XTask task);
     void Clear();
     CXFilter();
     ~CXFilter();
    QMutex mutex;
    std::vector<XTask>tasks;
};
#endif // XFILTER_H
