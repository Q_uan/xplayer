#include "XVideoThread.h"
#include <QMetaType>
#include "XFilter.h"

//一号视频源
static VideoCapture cap1;
static VideoCapture cap2;
XVideoThread::XVideoThread()
{
    qRegisterMetaType<cv::Mat>("Mat");

    //start();
}

 XVideoThread::~XVideoThread()
{

}
void XVideoThread::run()
{
    Mat mat1, bsmaskKNN;
    Mat des;
    //Mat kernel=getStructuringElement(MORPH_RECT,Size(3,3),Point(-1,-1));
    //Ptr<BackgroundSubtractorKNN> pKNN=createBackgroundSubtractorKNN();
   //bsmaskKNN.create(640,360,CV_8UC3);
   // namedWindow("shit",CV_WINDOW_AUTOSIZE);
    //moveWindow("shit",900,400);
    while(runFlag)
    {
         mutex.lock();
        /*
        if(!cap1.isOpened())
        {
            mutex.unlock();
            msleep(5);
            continue;
        }*/
        if(!isPlay)
        {
            mutex.unlock();
            msleep(10);
            continue;
        }
        int cur=cap1.get(CAP_PROP_POS_FRAMES);
        if(( end>0&& cur>=end)||!cap1.read(mat1) ||mat1.empty())
        {
            mutex.unlock();
            if(isWrite)
            {
                StopSave();
                emit SaveEnd();
            }
            msleep(5);
            continue;
        }
        else
        {
           //  pKNN->apply(mat1,bsmaskKNN);
           // circle(mat1,Point(100,100),50,Scalar(255,0,0),-1);
            mutex.unlock();
            if(!isWrite)
            {
                emit ViewImage1(mat1);
            }
            Mat mat2=mark;
            if(cap2.isOpened())
            {
                cap2.read(mat2);
            }
            des=XFilter::GetInstance()->Filter(mat1,mat2);
            if(!isWrite)
            {
                ViewDes(des);
                if(!mat2.empty())
                {
                    ViewImage2(mat2);
                }
            }


            if(isWrite)
            {
                vw.write(des);
            }
        }
        int times=0;
        times=1000/fps;
        if(isWrite) times=0;
        msleep(times);
    }
    cap1.release();
    deleteLater();
}
bool XVideoThread::Open(const QString file)
{
    Seek(0);
    mutex.lock();
    bool ret=cap1.open(file.toStdString());
   // bool ret=cap1.open(0);
    mutex.unlock();
    qDebug()<<ret<<endl;
    if(ret!=false)
    {
        fps=static_cast<int >(cap1.get(CAP_PROP_FPS));
        width=cap1.get(CAP_PROP_FRAME_WIDTH);
        height=cap1.get(CAP_PROP_FRAME_HEIGHT);
        qDebug()<<"fps="<<fps;
        if(fps<=0) fps=25;
        srcfile=file.toStdString();
        double count=cap1.get(CAP_PROP_FRAME_COUNT);
        totalMs=(count/(double)fps)*1000;
        return true;
    }

    return ret;
}
bool XVideoThread::Open2(const QString file)
{
    Seek(0);
    mutex.lock();
    bool ret=cap2.open(file.toStdString());
    mutex.unlock();
    qDebug()<<" XVideoThread::Open2 ret="<<ret<<endl;

    if(ret==true)
    {
        width2=cap2.get(CAP_PROP_FRAME_WIDTH);
        height2=cap2.get(CAP_PROP_FRAME_HEIGHT);
    }
    return ret;
}
void XVideoThread::stop()
{
    runFlag=false;
}
XVideoThread* XVideoThread:: Get()
{
    static XVideoThread vt;
    return &vt;
}
double XVideoThread::GetPos(void)
{
    double pos=0;
    mutex.lock();

    if(!cap1.isOpened())
    {
        mutex.unlock();
        return pos;
    }
    double count=cap1.get(CAP_PROP_FRAME_COUNT);
    double cur=cap1.get(CAP_PROP_POS_FRAMES);
    if(count>0.001)
        pos=cur/count;


    mutex.unlock();
    return pos;
}
 bool XVideoThread::Seek(int frame)
 {
     mutex.lock();
     if(!cap1.isOpened())
     {
         mutex.unlock();
         return false;
     }
     int ret=cap1.set(CAP_PROP_POS_FRAMES,frame);
     if(cap2.isOpened())
     {
         cap2.set(CAP_PROP_POS_FRAMES,frame);
     }

     mutex.unlock();
     return ret;
 }
bool XVideoThread::Seek(double pos)
{
    double count=cap1.get(CAP_PROP_FRAME_COUNT);
    int frame=static_cast<int >(pos*count);
    return  Seek(frame);
}

bool XVideoThread::StartSave(const std::string filename,int width,int height,bool isColor)//开始保存视频
{
   bool ret=false;
  qDebug()<<"start export "<<endl;
  Seek(begin);
  mutex.lock();
  if( !cap1.isOpened())
  {
      mutex.unlock();
      return false;
  }
      if(width<=0)
          width=cap1.get(CAP_PROP_FRAME_WIDTH);
      if(height<=0)
          height=cap1.get(CAP_PROP_FRAME_HEIGHT);
    qDebug()<<"before open"<<endl;
     qDebug()<<"width:"<<width<<endl;
      qDebug()<<"height:"<<height<<endl;
    //qDebug()<<"filename:"<<QString(filename)<<endl;
    ret=vw.open(filename,
            CV_FOURCC('X','2','6','4'),
            this->fps,
            Size(width,height),
            isColor
            );
    if(ret==false)
    {
        mutex.unlock();
        return ret;
    }

    qDebug()<<"vw.open()-->ret:"<<ret<<endl;
    this->isWrite=true;
     desfile=filename;
    mutex.unlock();
    return true;
}
void  XVideoThread::StopSave()
{
    mutex.lock();
    qDebug()<<"停止导出"<<endl;
    vw.release();
    isWrite=false;
    mutex.unlock();
}
void XVideoThread::SetMark(Mat mark)
{
    mark.copyTo(this->mark);
}
void XVideoThread::SetBegin(double p)
{
    mutex.lock();
    double count=cap1.get(CAP_PROP_FRAME_COUNT);
    int frame=p*count;
    begin=frame;
    mutex.unlock();
}
void XVideoThread::SetEnd(double p)
{
    mutex.lock();
    double count=cap1.get(CAP_PROP_FRAME_COUNT);
    int frame=p*count;
    end=frame;
    mutex.unlock();
}
