#ifndef XVIDEOUICLASS_H
#define XVIDEOUICLASS_H

#if QT_VERSION >= 0x050000
#include <QtWidgets/QWidget>
#else
#include <QWidget>
#endif
#include <QTimer>
namespace Ui {
class XVideoUIClass;
}

class XVideoUIClass : public QWidget
{
    Q_OBJECT

public:
    explicit XVideoUIClass(QWidget *parent = 0);
    ~XVideoUIClass();

private:
    Ui::XVideoUIClass *ui;
    QTimer  timer;
public slots:
    void Open();
    void close();
    void Set();
    void FreshSlider();
    void SliderPress();
    void SliderRelease();
    void Export();
    void Mark();
    void Blend();
    void Merge();

    void SetPos(int);
    void ExportEnd();//导出结束
    void Play();
    void Pause();
    void LeftSlider(int);
    void RightSlider(int);

private slots:
    void on_playSlider_actionTriggered(int action);
};

#endif // XVIDEOUICLASS_H
