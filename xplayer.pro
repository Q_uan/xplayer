#-------------------------------------------------
#
# Project created by QtCreator 2019-07-17T13:21:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = xplayer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        XVideoUIClass.cpp \
    XVideoWidget.cpp \
    XVideoThread.cpp \
    XImagePro.cpp \
    XFilter.cpp \
    XAudio.cpp

HEADERS += \
        XVideoUIClass.h \
    XVideoWidget.h \
    XVideoThread.h \
    XImagePro.h \
    XFilter.h \
    XAudio.h

FORMS += \
        XVideoUIClass.ui


INCLUDEPATH += $$PWD/../install/include
DEPENDPATH +=  $$PWD/../install/include


unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_core
unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_imgproc
unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_highgui
unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_videoio
unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_imgcodecs
unix:!macx: LIBS += -L$$PWD/../install/lib/ -lopencv_video


INCLUDEPATH += $$PWD/../../ffmpeg/install/include
DEPENDPATH += $$PWD/../../ffmpeg/install/include
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lavcodec
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lavdevice
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lavfilter
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lavformat
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lavutil

unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lswresample
unix:!macx: LIBS += -L$$PWD/../../ffmpeg/install/lib/ -lswscale



unix:!macx: LIBS += -L/usr/local/lib/ -lx264
INCLUDEPATH += $$PWD/../../x264/install/include
DEPENDPATH += $$PWD/../../x264/install/include




